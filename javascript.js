// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.getElementsByTagName('p');
console.log(paragraphs);
for (var i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = "#ff0000";
};



// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const findId = document.getElementById('optionsList');
console.log(findId);

console.log(findId.parentElement);

for (let node of findId.childNodes) {
    console.log(node.nodeName, node.nodeType);
};



// Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph
const paragraph = document.getElementsByClassName('testParagraph');
paragraph.innerHTML = 'This is a paragraph';
console.log(paragraph);



// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const findParent = document.querySelector('.main-header');
const findChild = findParent.children;
console.log(findChild);

for (let elem of findChild) {
    elem.classList.add('nav-item');
}
console.log(findChild);




// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const findElem = document.querySelectorAll('.section-title');
console.log(findElem);

findElem.forEach((element) => {
    element.classList.remove('section-title');
});
console.log(findElem);
